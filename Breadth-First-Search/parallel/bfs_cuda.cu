
#include <petscsys.h>
#include "bfs.h"

#define CUDA_CHK(cerr) do {cudaError_t _cerr = (cerr); if ((_cerr) != cudaSuccess) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_LIB,"Cuda error");} while(0)

int BFSGraphGetEdgeArraysDevice(BFSGraph graph, size_t E,
                                int numDevices,
                                size_t numEdgesLocal[], int64_t (*eLocal_p[])[2])
{
  cudaError_t       cerr;

  PetscFunctionBeginUser;

  numEdgesLocal[0] = E;

  cerr = cudaMalloc(&eLocal_p[0], E * sizeof(*eLocal_p[0])); CUDA_CHK(cerr);

  for (int i = 1; i < numDevices; i++) {
    numEdgesLocal[i] = 0;
    eLocal_p[i] = NULL;
  }

  PetscFunctionReturn(0);
}

int BFSGraphRestoreEdgeArraysDevice(BFSGraph graph, size_t E,
                                    int numDevices,
                                    size_t numEdgesLocal[], int64_t (*eLocal_p[])[2])
{
  cudaError_t    cerr;

  PetscFunctionBeginUser;
  cerr = cudaFree(eLocal_p[0]); CUDA_CHK(cerr);
  PetscFunctionReturn(0);
}

int BFSGraphSetEdgesDevice(BFSGraph graph, size_t E, int numDevices,
                           const size_t numEdgesLocal[],
                           const int64_t (*eLocal_p[])[2])
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}

int BFSGraphGetParentArraysDevice(BFSGraph graph, int numDevices, size_t numVerticesLocal[],
                                  int64_t firstLocalVertex[], int64_t *parentsLocal[])
{
  PetscFunctionBeginUser;

  for (int i = 0; i < numDevices; i++) {
    numVerticesLocal[i] = 0;
    firstLocalVertex[i] = 0;
    parentsLocal[i] = NULL;
  }
  PetscFunctionReturn(0);
}

int BFSGraphRestoreParentArraysDevice(BFSGraph graph, int numDevices, size_t numVerticesLocal[],
                                      int64_t firstLocalVertex[], int64_t *parentsLocal[])
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}

int BFSGraphSearchDevice(BFSGraph graph, int num_keys, const int64_t *key, int numDevices, const size_t numVerticesLocal[], const int64_t firstLocalVertex[], int64_t **parentsLocal[])
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}

/* vi: set expandtab sw=2 ts=2 cindent: */
