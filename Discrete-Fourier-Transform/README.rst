=============================
3D Discrete Fourier Transform
=============================

:Author: CSE 6230
:Date:   Fall 2017

Definition
==========

Given an array :math:`x \in \mathbb{C}^{N\times N \times N}`, where :math:`N`
is a power of 2, the *unitary 3D discrete Fourier transform* of :math:`x` is
:math:`y\in\mathbb{C}^{N \times N \times N}` given by

.. math::

  y_{\bar{\jmath}} = \frac{1}{N^{3/2}}\sum_{\bar{k}} x_{\bar{k}} \mathrm{e}^{-
  \mathrm{i} 2 \pi \bar{\jmath} \cdot \bar{k} / N},

where :math:`\bar{\jmath}` and :math:`\bar{k}` are triples and :math:`0\leq
j_l, k_l < N` for all :math:`l`.
