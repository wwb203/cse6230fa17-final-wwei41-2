#include "dft.h"
#include <petscsys.h>

struct _dft
{
  MPI_Comm comm;
};

int DFTCreate(MPI_Comm comm, DFT *dft_p)
{
  DFT dft = NULL;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscCalloc1(1,&dft); CHKERRQ(ierr);

  dft->comm = comm;

  *dft_p = dft;
  PetscFunctionReturn(0);
}

int DFTDestroy(DFT *dft_p)
{
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  ierr = PetscFree(*dft_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int DFTGetFieldArrays(DFT dft, size_t N, struct IndexBlock *iBlock, double _Complex **xLocal_p, double _Complex **yLocal_p)
{
  MPI_Comm comm;
  size_t   local;
  int      size, rank;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  comm = dft->comm;

  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);

  iBlock->xStart = 0;
  iBlock->xEnd   = N - 1;
  iBlock->yStart = 0;
  iBlock->yEnd   = N - 1;
  iBlock->zStart = (rank * (N - 1)) / size;
  iBlock->zEnd   = ((rank + 1) * (N - 1)) / size;

  local = (iBlock->xEnd - iBlock->xStart) * (iBlock->yEnd - iBlock->yStart) * (iBlock->zEnd - iBlock->zStart);

  ierr = PetscMalloc2(local, xLocal_p, local, yLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int DFTRestoreFieldArrays(DFT dft, size_t N, struct IndexBlock *iBlock, double _Complex **xLocal_p, double _Complex **yLocal_p)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscFree2(*xLocal_p, *yLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int DFTTransform(DFT dft, size_t N, struct IndexBlock *iBlock, const double _Complex *xLocal, double _Complex *yLocal)
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}
