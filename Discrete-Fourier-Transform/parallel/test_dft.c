const char help[] = "Test driver for the correctness of discrete Fourier transform implementation";

#include <petscviewer.h>
#include <fftw3.h>
#include "dft.h"

int main(int argc, char **argv)
{
  PetscInt       test, numTests = 10;
  PetscInt       scale = 15;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Discrete Fourier Test Options", "test_dft.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_tests", "Number of tests to run", "test_dft.c", numTests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-scale", "Scale (log2) of the array in the test", "test_dft.c", scale, &scale, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of discrete_fourier_transform()\n", numTests);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscComplex *x, *y, *ycheck;
    PetscReal     diff;
    PetscReal     norm;
    PetscInt      n, i;
    int           size, rank;
    size_t        localSize;
    struct IndexBlock ib;
    DFT           dft = NULL;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);

    n = 1 << (scale / 3);
    norm = PetscPowReal((PetscReal) n, -3./2.);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: [%D x %D x %D]\n", n, n, n);CHKERRQ(ierr);

    ierr = DFTCreate(comm, &dft);CHKERRQ(ierr);
    ierr = DFTGetFieldArrays(dft, n, &ib, &x, &y); CHKERRQ(ierr);

    localSize = (ib.xEnd - ib.xStart) * (ib.yEnd - ib.yStart) * (ib.zEnd - ib.zStart);
    ierr = PetscMalloc1(localSize, &ycheck);CHKERRQ(ierr);

    ierr = PetscRandomSetInterval(rand, -1. + -1. * PETSC_i, 1. + 1 * PETSC_i);CHKERRQ(ierr);

    for (i = 0; i < localSize; i++) {
      PetscReal re, im;

      ierr = PetscRandomGetValueReal(rand, &re);CHKERRQ(ierr);
      ierr = PetscRandomGetValueReal(rand, &im);CHKERRQ(ierr);
      x[i] = re + PETSC_i * im;
    }

    ierr = DFTTransform(dft, n, &ib, x, y);CHKERRQ(ierr);

#if 0
    {
      fftw_plan p;

      p = fftw_plan_dft_3d(n, n, n, (fftw_complex *) x, (fftw_complex *) ycheck,FFTW_FORWARD, FFTW_ESTIMATE);

      fftw_execute(p);

      fftw_destroy_plan(p);
    }

    diff = 0.;
    for (i = 0; i < n*n*n; i++) {
      PetscComplex res = y[i] - norm * ycheck[i];

      diff += PetscRealPart(res * PetscConjComplex(res));
    }
    diff = PetscSqrtReal(diff);

    if (diff > PETSC_SMALL) SETERRQ3(comm, PETSC_ERR_LIB, "Test %D failed residual test at threshold %g with value %g\n", test, (double) PETSC_SMALL, (double) diff);
#endif

    ierr = PetscFree(ycheck);CHKERRQ(ierr);
    ierr = DFTRestoreFieldArrays(dft, n, &ib, &x, &y); CHKERRQ(ierr);
    ierr = DFTCreate(comm, &dft);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscViewerASCIIPrintf(viewer, "Passed.\n");CHKERRQ(ierr);


    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
